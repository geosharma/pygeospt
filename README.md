# A simple package to compute correction factors for Geotechnical Standard Penetration Test (SPT) N values

This will convert standard penetration test (SPT) N values measured in the field to N160 values which can then be used in further analysis.

Corrections applied:
 - Hammer energy correction
 - Borhole size correction
 - Rod length correction
 - Overburden correction

This is a work in progress.

## Keywords:
 - Geotechnical Engineering
 - Soil Mechanics
 - Soil Characterization
 - Geotechincal Field Exploration
 - Standard Penetration Tests
 - SPT Blow Counts, N-value
 - SPT Corrections
 
## Documentation
This will be the eventual location of documentation but as of now I haven't generated the documentations yet. 
[pygeospt documentation](https://pygeospt.readthedocs.io/en/latest/)


## Usage
Sorry for the lack of documentation. I eventually plan to have some documentation with examples. Untill then please refer to the `tests` and the `examples` folders. These are not yet organized well but may help.  

## Requirements
There is a *requirements.txt* file which was generated using `pipreqs`. The version number may not be very important. Required packages are `numpy`, and my other package to compute vertical effective stress `pyvstress`. The version numbers my not be important.
