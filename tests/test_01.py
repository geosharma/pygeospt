from pyvstress import Point, SoilLayer, SoilProfile
from pygeospt import SPT


def main():
    # create a soil profile with three layers
    layer1 = SoilLayer(5, 110, 120)
    layer2 = SoilLayer(15, 115, 122)
    layer3 = SoilLayer(20, 120, 125)
    soilprofile = SoilProfile([layer1, layer2, layer3], zw=10)

    # depths of interest
    zs = [0, 1, 2, 3, 4, 5, 9, 10, 11, 15, 20, 25, 30, 40]
    pts = [Point(z) for z in zs]

    soilprofile.vertical_stresses(pts)

    for pt in pts:
        print(pt.z, pt.effective_stress)

    # for pt in pts:
    #     print(pt.z, pt.total_stress, pt.pore_pressure, pt.effective_stress)
    effstresses = [pt.effective_stress for pt in pts]
    spt = SPT()
    crs = spt.rod_length_correction_factor(zs)
    cns = spt.overburden_correction_factor_LW1986(effstresses)
    print(len(effstresses))
    print(cns.shape)
    print(cns[-1])
    print(zs[-1])
    print(effstresses[-1])

    
if __name__ == "__main__":
    main()
