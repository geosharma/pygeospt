import numpy as np
import warnings


def overburden_correction_exponent_IB2008(n160cs):
    """Return overburden correction factor exponent

    :param float n160cs: clean sand corrected n160

    :returns: m
    :rtype: float
    """
    n160cs = np.clip(n160cs, 0, 46)
    return 0.784 - 0.0768 * np.sqrt(n160cs)


def overburden_correction_factor_IB2008(m, effective_stress, pa=2116.22):
    """Overburden correction factor Idriss and Boulanger (2010), <= 1.7
    effective_stress: float, vertical effective stress at specified depth
    N160cs: float, equivalent clean sand corrected blow count, N160cs <= 46
    Return the correction factor, float
    """

    return np.clip(np.power(pa / effective_stress, m), 0, 1.7)


def clean_sand_correction_IB2008(fc):
    """Equivalent clean sand adjustment

    :param float fc: fines content in percent"""

    return np.exp(1.63 + 9.7 / (fc + 0.01) - (15.7 / (fc + 0.01)) ** 2)


def calc_overburden_correction_factor(effective_stress, N160cs, pa=2116.22):
    """Overburden correction factor: K sigma
    ref: Boulanger, R. W. (2003)
    ref: Idriss, I. M., and Boulanger, R. W. (2008). Soil liquefaction during earthquakes. Monograph MNO-12,
    Earthquake Engineering Research Institute, Oakland, CA, 261 pp.
    effective_stress: float
    N160cs: float, equivalent clean sand blow count
    units: string, eithre "kNm" or "lbft"
    """

    csigma = np.clip(1 / (18.9 - 2.55 * np.sqrt(N160cs)), None, 0.3)

    return np.clip(1.0 - csigma * np.log(effective_stress / pa), None, 1.1)


def overburden_correction_exponent_iter(
    n60, dn160, effective_stress, pa=2116.22, maxiter=10, tol=1e-4
):
    """Iteration for computing overburden correction factor exponent

    :param float n60: spt N value correction for energy, borehole size, rod length, and sample size.
    :param float dn160: clean sand correction for spt N value
    :param float effective_stress: effective stress at the depth of spt N value
    :param float pa: value of the atmospheric pressure
    :param int maxiter: maximum number of iterations before exiting
    :param float tol: convergence tolerance

    :returns: overburden correction factor
    :rtype: float
    """
    n160 = n60
    for i in range(maxiter):
        n160cs = np.clip(n160 + dn160, 0, 46)
        m = overburden_correction_exponent_IB2008(n160cs)
        n160 = overburden_correction_factor_IB2008(m, effective_stress) * n60
        n160cs = np.clip(n160 + dn160, 0, 46)
        if abs(m - overburden_correction_exponent_IB2008(n160cs)) < tol:
            break

    if i == maxiter - 1:
        warnings.warn(f"i = {i}, Reached maximum iteration of {maxiter}")

    return (i, m)


def main():
    # fines content
    pa = 2116.22
    sigeff = 8.0 * pa
    fc = 15
    n60 = 10
    dn160 = clean_sand_correction_IB2008(fc)
    tol = 1e-5
    i, m = overburden_correction_exponent_iter(n60, dn160, sigeff, tol=tol)
    print(f"i = {i:d}, m = {m:.3f}")

    n160cs = n60 + dn160
    print(f"""{"i":>3s}, {"m":>5s}, {"cn":>5s}, {"n160":>5s}, {"n160cs":>5s}""")
    for i in range(10):
        m = overburden_correction_exponent_IB2008(n160cs)
        cn = overburden_correction_factor_IB2008(m, sigeff)
        n160 = cn * n60
        n160cs = n160 + dn160
        print(
            f"{i:3d}, {m:5.3f}, {cn:5.2f}, {n160:5.2f}, {n160cs:5.2f}, {abs(m - overburden_correction_exponent_IB2008(n160cs))}"
        )
        if abs(m - overburden_correction_exponent_IB2008(n160cs)) < tol:
            break
    # result = minimize(
    #         overburden_correction_objectivefunction,
    #         bracket=(0.3, 0.784),
    #         args=(n60, dn160, sigeff),
    #         method="brent",
    #     )