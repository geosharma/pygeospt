import numpy as np


class SPT:
    def __init__(
        self,
        hammer_efficiency=0.60,
        borehole_diameter=100,
        rod_length=3.0,
        sampler="standard",
    ):
        """Define test parameters for the SPT

        :param float hammer_efficiency: energy efficiency of the hammer system in decimal e.g. 0.60 for 60%
        :param float borehole_diameter: diameter of the borehole in mm
        :param float rod_length: single rod length in feet
        :param str sampler: type of sampler, either: "standard", or "without_liner"
                           in most cases "standard",
                           samplers to be used with liners but used without liners "without_liner"
        
        The corrections are based on Table 3 Corrections factor for SPT N values, pg 73, in Idriss and Boulanger (2008)
        Soil Liquefaction During Earthquake.
        """

        self._hammer_efficiency = hammer_efficiency
        self._borehole_diameter = borehole_diameter
        self._single_rod_length = rod_length
        self._sampler = sampler

        # hammer energy correction factor
        self._ce = self.energy_correction()
        # borehole diameter correction factor
        self._cb = self.borehole_dia_correction()
        # sampler correction factor
        self._cs = self.sampler_correction_factor()
        # rod_length correction factor
        self._cr = []

    @property
    def ce(self):
        """Return the energy correction factor in decimal."""
        return self._ce

    @property
    def cb(self):
        """Return the borehole correction factor"""
        return self._cb
    
    @cb.setter
    def cb(self, x):
        "Set the value of borehole correction factor"
        self._cb = x 
    
    @property
    def cs(self):
        """Return the value of sampler correction factor"""
        return self._cs

    @cs.setter
    def cs(self, x):
        """Set the value of the sampler correction factor"""
        self._cs = x

    def energy_correction(self):
        """Correction factor for hammer efficiency

        Typical range of hammer correction:
        Automatic trip hammer: 0.8 - 1.5
        Safety hammer: 0.7 - 1.2
        Donut hammer: 0.5 - 1.0

        :returns: hammer energy correction factor, ce
        :rtype: float
        """

        if 0.0 > self._hammer_efficiency > 1.0:
            raise ValueError(
                "Hammer efficiency must be 0.0 to 1.0; efficiency cannot be smaller than 0.0 or higher than 1.0 (100%)"
            )
        else:
            return self._hammer_efficiency / 0.60

    def borehole_dia_correction(self):
        """Correction factor for non-standard borehole diameter

        Borehole parameters, Skempton (1986)
        |BH Dia. [mm] |   Correction Factor |
        |:------------|--------------------:|
        |60 - 120 mm  | 1.0                 |
        |150 mm       | 1.05                |
        |200 mm       | 1.15                |

        :returns: borehole correction factor, cb
        :rtype: float

        If the borehole diameter and correction factors do not correspond to the above table, use spt.cb method to set the value.
        """
        tol = 0.1
        if (60 - tol) <= self._borehole_diameter <= (120 + tol):
            return 1.0
        elif abs(150 - self._borehole_diameter) <= tol:
            return 1.05
        elif abs(200 - self._borehole_diameter) <= tol:
            return 1.15
        else:
            raise ValueError(
                "Borehole diameter outside valid range. Please check documentation."
            )

    def sampler_correction_factor(self):
        """Sampler correction factor for non-standard sampler

        :returns: cs, sampler correction factor
        :rtype: float

        Sampler correction factors, Skempton (1986)
        |Sampler                       |   Correction Factor |
        |:-----------------------------|--------------------:|
        | Standard                     |    1.0              |
        | With liner: loose sand       |    1.0              |
        | With liner: dense sand, clay |    1.0              |
        | Without liner                |    1.2

        Use "standard" for all the three cases with correction factor = 1.0, else use "without_liner".

        :returns: correction factor non-standard sampler
        :rtype: float

        If a correction factor other than those given in the above table is to be used then use spt.
        """

        if self._sampler == "without_liner":
            return 1.2
        else:
            return 1.0

    def rod_length_correction_factor(self, z, stickup=0.0):
        """Correction factor for rod length

        :param float z: depth of the SPT
        :param float single_rod_length: length of single rod in ft
        :param float stickup: stickup length in ft

        If the value of stickup is greater than the default value of 0.0, single_rod_length will be ignored and only stickup value will be used.
        When stickup is used rod length is calculated as depth of point below ground surface plus stickup length.

        Rod length correction factors, Youd et al. (2001)
        |Rod length [ft] |   Correction Factor |
        |:---------------|--------------------:|
        | 0 - 10 ft      | 0.75                |
        | 10 - 13 ft     | 0.80                |
        | 13 - 20 ft     | 0.85                |
        | 20 - 30 ft     | 0.95                |
        | 30 - 100 ft    | 1.0                 |
        | 100+           | 1.0                 |

        :returns: rod length correction factor, cr
        :rtype: float
        """

        z = np.asarray(z, dtype=float)
        correction_factors = np.array([0.75, 0.80, 0.85, 0.95, 1.0], dtype=np.float)
        bins = np.array([10, 13, 20, 30], dtype=np.float)

        # given the length of single rod, rod_length, the number of rods used for given depth, z
        quotient = z // self._single_rod_length
        remainder = z % self._single_rod_length
        nrods = np.where(remainder > 0, quotient + 1, quotient)
        rod_length = nrods * self._single_rod_length

        # if stickup is greater than 0.0, means ignore single rod and use stickup instead.
        if stickup > 0.1:
            rod_length = z + stickup

        idx = np.searchsorted(bins, rod_length, side="right")
        return correction_factors[idx]

    def overburden_correction_factor_LW1986(self, effective_stress, pa=2116.22):
        """Overburden correction factor for standard penetration tests
        Return the correction factor, float <= 2.0
        ref: Liao and Whitman (1986), get full reference

        :param float effective_stress: vertical effective stress at specified depth
        :param float pa: atmospheric pressure
        
        :returns: cn
        :rtype: float
        """
        effective_stress = np.asarray(effective_stress)
        effective_stress[effective_stress < 0.001] = 0.001
        return np.clip((pa / effective_stress) ** 0.5, 0, 2.0)


    def overburden_correction_exponent_IB2008(n160cs):
        """Return overburden correction factor exponent

        :param float n160cs: clean sand corrected n160

        :returns: m
        :rtype: float
        """
        n160cs = np.clip(n160cs, 0, 46)
        return 0.784 - 0.0768 * np.sqrt(n160cs)

    def overburden_correction_factor_IB2008(m, effective_stress, pa=2116.22):
        """Overburden correction factor Idriss and Boulanger (2010), <= 1.7
        
        :param float m: float, overburden correction factor exponent
        :param float effective_stress: vertical effective stress at specified depth
            
        :returns: overburden correction exponent
        :rtype: float
        """
        
        return np.clip(np.power(pa / effective_stress, m), 0, 1.7)


    def SPT_rod_length_correction_factor_Cetin2018(z, single_rod_length, units):
        """Correction factor for rod length
        rod_length: float, length, in meter, of the rod including the stick up above the ground

        """

        # given the length of single rod, rod_length, the number of rods used for given depth, z
        quotient = z // single_rod_length
        remainder = z % single_rod_length

        nrods = np.where(remainder > 0, quotient + 1, quotient)
        rod_length = nrods * single_rod_length

        if units == "lbft":
            rod_length = rod_length * 0.3048

        # if rod_length > 30:
        #     raise ValueError(
        #         "Rod Length in SPT_rod_length_correction_factor_Cetin2018 greater than 30m. Rod lenght should be less than 30m"
        #     )
        cr = np.where(rod_length <= 10, 0.48 + 0.225 * np.log(rod_length), 0.48)
        return


    def shear_wave_velocity_from_SPT_JapanRoad(N60, soil_type):
        """Relationship for estimating shear wave velocity from SPT blow counts
        ref: Cetin et al. (2018). The use of the SPT-based seismic soil liquefaction triggering
            evaluation methodology in engineering hazard assessments. MethodsX 5 (2018) 1556–1575.
        and  Japan Road Association Design Specifications for Highway Bridges, (2002)
        """
        mask = soil_type == "clay"
        vs = 80 * N60 ** (1 / 3)
        vs[mask] = 100 / 80 * vs[mask]
        return vs


    def calc_dN160(fc):
        """Equivalent clean sand adjustment
        fc: float, fines content in percent"""
        return np.exp(1.63 + 9.7 / (fc + 0.01) - (15.7 / (fc + 0.01)) ** 2)


    def SPT_overburden_correction_objectivefunction(
        N160, N60, dN160, effective_stress, units
    ):
        N160cs = N160 + dN160
        # print(
        #     f"N60: {N60:3.1f}, N160: {N160:3.3f}, dN160: {dN160:3.2}, N160cs: {N160cs:3.3f}"
        # )
        Cn = SPT_overburden_correction_factor_IB2008(N160cs, effective_stress, units)
        # print("CN: ", CN)
        diff = np.abs(N160 - N60 * Cn)
        # print("diff: ", diff)
        return diff


    def SPT_corrected_blow_counts(sptN, Ce, Cr, effective_stress, dN160, units="kNm"):
        """Corrected SPT blow counts, N160.
        Field blow counts corrected for hammer_energy, overburden, rod length
        N: float, Uncorrected field SPT blow count,
        hammer_energy: float, hammer energy in percent,
        effective_stress: float, overburden effective stress at blow count location,
        N160cs: float, equivalent clean sand N160,
        single_rod_length: float, length of rod to the depth of SPT blow counts including rod stick up,
        pa: float, atmospheric pressure,
        Return
        """

        # equivalent clean sand adjustment for fines content
        # dN160 = calc_dN160(fc)

        # correct the Nvalue for hammer energy and rod length
        # Ce = SPT_energy_correction(hammer_efficiency)
        # Cr = SPT_short_rod_correction_factor(z, single_rod_length, units=units)

        # correct for hammer energy and short rod length
        sptN = sptN * Ce * Cr

        # list for return values
        n160 = []
        for sptn, dn160, sigma_eff in zip(sptN, dN160, effective_stress):
            # correct for overburden, iterative process
            result = minimize_scalar(
                SPT_overburden_correction_objectivefunction,
                bracket=(sptn / 2, 1.2 * sptn),
                args=(sptn, dn160, sigma_eff, units),
                method="brent",
            )
            # N160 = round(result.x)
            # # print(f"N160cs: {N160cs}")
            # N160cs = N160 + dn160
            # Cn = SPT_overburden_correction_factor(N160cs, sigma_eff, pa)
            # rvals["N160cs"].append(round(N160cs))
            # rvals["Cn"].append(Cn)
            n160.append(result.x)
        return n160


    def CRR_normalized(N160cs):
        """Cyclic resistance ratio (CRR) adjusted to M = 7.5 and 1 atm effective stress"""
        return np.exp(
            N160cs / 14.1
            + (N160cs / 126) ** 2
            - (N160cs / 23.6) ** 3
            + (N160cs / 25.4) ** 4
            - 2.8
        )


    def Idriss1999_magnitude_scaling_factor(M):
        """Magnitude scaling factor for magnitudes other than Mw=7.5.
        ref: Idriss, I. M. (1999). An update to the Seed-Idriss simplified procedure for evaluating liquefaction
        potential, in Proceedings, TRB Workshop on New Approaches to Liquefaction, Publication No. FHWARD-
        9-165, Federal Highway Administration, January.
        ref: Idriss, I. M., and Boulanger, R. W. (2008). Soil liquefaction during earthquakes. Monograph MNO-12,
        Earthquake Engineering Research Institute, Oakland, CA, 261 pp.
        M: float, earthquake magnitude most probably moment magnitude Mw
        Return, float, magnitude correction factor
        """
        return np.clip(6.9 * np.exp(-M / 4) - 0.058, None, 1.8)


    def BI2014_magnitude_scaling_factor(M, N160cs):
        """Boulanger and Idriss (2014) Magnitude scaling factor for earthquake magnitudes other than Mw = 7.5
        ref: Boulanger, R. W., and Idriss, I. M. (2014). CPT and SPT Based Liquefaction Triggering Procedures. Rep. UCD/CGM-14/01
        M: float, earthquake magnitude most probably moment magnitude, Mw
        N160cs: float, equivalent clean sand blow counts
        """
        msfmax = np.clip(1.09 + (N160cs / 31.5) ** 2, None, 2.2)
        return 1 + (msfmax - 1) * (8.64 * np.exp(-M / 4) - 1.325)


    def calc_overburden_correction_factor(effective_stress, N160cs, units):
        """Overburden correction factor: K sigma
        ref: Boulanger, R. W. (2003)
        ref: Idriss, I. M., and Boulanger, R. W. (2008). Soil liquefaction during earthquakes. Monograph MNO-12,
        Earthquake Engineering Research Institute, Oakland, CA, 261 pp.
        effective_stress: float
        N160cs: float, equivalent clean sand blow count
        units: string, eithre "kNm" or "lbft"
        """

        if units == "kNm":
            pa = 101.325
        elif units == "lbft":
            pa = 2116.22
        else:
            print("ERROR: WRONG UNITs in SPT corrected blow counts")

        csigma = np.clip(1 / (18.9 - 2.55 * np.sqrt(N160cs)), None, 0.3)

        return np.clip(1.0 - csigma * np.log(effective_stress / pa), None, 1.1)


    def calc_CRR(effective_stress, N160cs, M, units):
        """Cyclic resistance ratio"""

        return (
            CRR_normalized(N160cs)
            * CRR_magnitude_scaling_factor(M)
            * CRR_overburden_correction_factor(effective_stress, N160cs, units)
        )


    def CSR_stress_reduction_factor(M, z, units):
        """Stress reduction factor to account for dynamic response of a soil profile.
        M, float, Earthquake magnitude, most probably Moment magnitude Mw
        z, float, depth below the soil surface,
        Return correction factor
        """
        if units == "kNm":
            pass
        elif units == "lbft":
            z = z * 0.3048
        else:
            print(f"ERROR: {units}, Wrong units key in CSR_stress_reduction_factor")

        alpha = -1.012 - 1.126 * np.sin(z / 11.73 + 5.133)
        beta = 0.106 + 0.118 * np.sin(z / 11.28 + 5.142)

        return np.exp(alpha + beta * M)


    def calc_CSR(total_stress, effective_stress, amax, rd):
        """Cyclic stress ratio
        total_stress: float,
        effective_stress: float,
        amax: float, peak ground acceleration units of g
        rd: float, unitless, shear stress reduction factor to account dynamic response of the soil profile
        """
        return 0.65 * total_stress / effective_stress * amax * rd


def main():
    # tolerance for depths
    tol = 1e-3
    # paths
    fpath_root = Path(r"../")
    fpath_data = fpath_root / "data"

    # input file containing soil profile and SPT N value information
    fname_input = "soil_profile.txt"
    fin = fpath_data.joinpath(fname_input)
    fname_output = fin.stem + "_out.csv"
    fout = fpath_data.joinpath(fname_output)

    # reading the heading block
    project, units = read_heading(fin)
    print(f"Project: {project}, Units: {units}")

    # read spt setup
    hammer_efficiency, borehole_diameter, stick_up, rod_length = read_SPTsetup(fin)

    # groundmotion parameters
    # g = 9.81
    # magnitude = 7.8
    # amax = 0.2 * g
    magnitude, amax = read_earthquake_param(fin)
    # read input file
    soil_layers, dp, zw, zs, gammaw = read_inputfile(fin, tol, withSPT=False)

    print(f"Depth of profile, dp = {dp:3.3f}")
    print(f"Depth of water table, (negative is above ground), zw = {zw:3.3f}")
    print(f"Depth of saturation zone, zs = {zs:3.3f}")
    print(f"Unit weight of water, gammaw = {gammaw:3.3f}")

    # stresses = calc_stresses(layers, zw, zs, gammaw, tol)

    # locations to compute stresses, not required when we have data
    # zs = read_stress_locations(fin)
    # soil properties
    data = read_SPTFC(fin)
    zspt = np.array([dat["z"] for dat in data], dtype=np.float)
    sptN = np.array([dat["spt"] for dat in data], dtype=np.int8)
    fc = np.array([dat["fc"] for dat in data], dtype=np.float)

    # compute total stresses, pore water pressure and effective stresses
    stresses = calc_stresses(soil_layers, zspt, zw, zs, gammaw, tol)
    # print(stresses)
    # filter for zs
    colnames = ["z", "total_stress", "pore_pressure", "effective_stress"]
    df = pd.DataFrame(data=stresses, columns=colnames)
    df = df.loc[df["z"].isin(zspt)]
    # add data to the dataframe
    df["spt"] = sptN
    df["fc"] = fc
    df["saturated"] = np.where((df["z"] - zw) > 1e-3, True, False)
    df = df.loc[df["saturated"]]

    # first compute the Cyclic stress ratio at specified depths and a given peak ground acceleration
    df["rd"] = CSR_stress_reduction_factor(magnitude, df.loc[:, "z"].values, units)
    df["CSR"] = calc_CSR(
        df["total_stress"].values,
        df["effective_stress"].values,
        amax,
        df.loc[:, "rd"].values,
    )

    df["dN160"] = calc_dN160(df.loc[:, "fc"])
    df["dN160"] = np.where(df["dN160"] < 0.01, 0.0, df["dN160"])

    df["Ce"] = SPT_energy_correction(hammer_efficiency)
    df["Cr"] = SPT_short_rod_correction_factor(
        df.loc[:, "z"], rod_length, units=units, stickup=stick_up
    )

    df["N160"] = SPT_corrected_blow_counts(
        df["spt"], df["Ce"], df["Cr"], df["effective_stress"], df["dN160"], units=units
    )
    df["N160cs"] = df["N160"] + df["dN160"]
    df["Cn_IB2008"] = SPT_overburden_correction_factor_IB2008(
        df["N160cs"], df["effective_stress"], units
    )
    df["CRR_normalized"] = CRR_normalized(df["N160cs"])
    df["k_sigma"] = calc_overburden_correction_factor(
        df["effective_stress"], df["N160cs"], units
    )
    df["k_msf"] = BI2014_magnitude_scaling_factor(magnitude, df["N160cs"])
    df["CRR"] = df["CRR_normalized"] * df["k_msf"] * df["k_sigma"]
    # df["N160cs_check"] = df["spt"] * df["Cn"] * df["Ce"] * df["Cr"]
    df["FS"] = df["CRR"] / df["CSR"]
    print(df)
    # print(rd, CSR)
    # df.to_csv(fout, index=False, sep=",")
    # CR = SPT_short_rod_correction_factor(29, 4, units=units)
    # result = minimize_scalar(
    #     SPT_overburden_correction_objectivefunction,
    #     bracket=(1, 60),
    #     args=(50, 5, 120, 101.325),
    #     method="brent",
    # )
    # print(result.x)
    # hammer_efficiency, borehole_diameter, rod_length = read_SPTsetup(fin)
    # print(hammer_efficiency, borehole_diameter, rod_length)
    # N160, Ce, Cr, Cn, N160cs = SPT_corrected_blow_counts(
    #     30, 2, 63, 1.2192, 50, 10, units="kNm"
    # )


if __name__ == "__main__":
    main()
